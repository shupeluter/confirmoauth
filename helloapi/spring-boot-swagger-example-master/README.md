# Spring Boot integration with Swagger 2 UI

`http://localhost:8088/swagger-ui.html` - Shows the list of Endpoints in the current RESTful webservice.
Original Code is published in "https://github.com/TechPrimers/spring-boot-swagger-example"
