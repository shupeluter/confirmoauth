How to use
=====

# Environment

## Environment for execution

|item|vaue|
|----|----|
|host os|Windows|
|wsl guest os|Ubuntu 20.0.04|
|JDK|11|
|maven|3.x|
|virtual environment|wsl2|
|Contaner runtime|Docker desktop|

In this contents, each command MUST be executed on Ubuntu not on Host OS.

## Tools

|item|value|
|----|-----|
|post man|---|

On creating the system, we can use "Postman" to call some api.

## Prepare for servers.

### Setting up wsl

1. create %userprofile%\.wslconfig
2. add berrow conatints to .wslconfig

```
localhostForwarding=True
```

### Install packages for build the api project
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install openjdk-11-jdk-headless
sudo apt-get install maven
sudo apt-get install podman python3-pip
```

### Install podman-compose

``` python
pip3 install podman-compose
```

You should execute bellow command to confirm your environment is enough or not.

``` python
podman-compose --version

if you can watch message like below, your environent is enough.

['podman', '--version', '']
using podman version: 3.4.4
podman-composer version  1.0.3
podman --version
podman version 3.4.4
exit code: 0

```


### build API applicaton

```
mvn -f helloapi/spring-boot-swagger-example-master/pom.xml package
```

### Wake up the system.

```
podman-compose build
podman-compose up
```

### Keycloak Settings

Import these two files to postman.

|item|purpose|
|----|------|
|Oauth.postman_globals.json|Glovale environments to execute above collection|
|Inittial Settigs for Keycloak.postman_collection.json|Collection for creating realm, user, and clients|

After waked up your keycloawk, execute the "Inittial Settigs for Keycloak" collection on your Postman.


## On keycloak admin console

1. open your browser and access "http://localhost:9090/auth/"
1. open "Demo1" realm.
1. Slect "Clients".
1. Get client_secrets for app_client①, api_gw_client②

★ここの意味がイマイチ不明

```
curl http://localhost/auth/realms/demo1/protocol/openid-connect/token -d "grant_type=password&client_id=app_client&username=app1&password=passwd&scope=openid&client_secret=<client_secrete①>"

``` 
### Configure your api-gw
Change client secrete value indicated by "★".

``` 
[nginx/conf/conf.d/sample.conf]
    location = /_oauth2_send_request {
        internal;
        proxy_method      POST;
        proxy_set_header  Content-Type "application/x-www-form-urlencoded";
        proxy_set_body    "token=$access_token&token_hint=access_token&client_id=api_gw_client&<client secrete②>";★
        proxy_pass        http://authen:8080/auth/realms/demo1/protocol/openid-connect/token/introspect;
    }
```

#### restert api-gw
The end of your preparation, you should restert your api-gw

```
podman-compose stop nginx
podman-compose up nginx
```

# Let's experience the "introspection endpoint"

## Scenario 1: Access to your api without access token

```
$ curl http://localhost/rest/hello
<html>
<head><title>403 Forbidden</title></head>
<body>
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.21.1</center>
</body>
</html>
```

You cant acces your api without access token.

## Scenario 2: Access to your api with incorrect token

```
$ curl http://localhost/rest/hello -H "Authorization: Bearer hoge"
<html>
<head><title>403 Forbidden</title></head>
<body>
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.21.1</center>
</body>
</html>
```

## Scenario 3: Access to your api with correct token

### Get your access token
```
$ curl http://localhost/auth/realms/demo1/protocol/openid-connect/token -d "grant_type=password&client_id=app_client&client_secret=<client_secrete①>&username=app1&password=passwd&scope=openid"
{
"access_token":"...★",
"expires_in":300,
"refresh_expires_in":1800
"refresh_token":"...",
"token_type":"Bearer",
"id_token":"...",
"not-before-policy":0,"session_state":"0acf8c83-2a81-43a5-a404-71f5e16035f2",
"scope":"openid profile email"
}

```

### Access your api with token

```
$ curl http://localhost/rest/hello -H "Authorization: Bearer <Access Token>"
Hello World
```
Please after 5 minites execute same command with same token. you will not able to access your api becaouse the token will have bean expired.

### Reference
+ Keycloakのインストールと構築例:https://thinkit.co.jp/article/17621
