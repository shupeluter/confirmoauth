# Setting up podman 
## Setting Repositries

```conf
# # An array of host[:port] registries to try when pulling an unqualified image, in order.
# unqualified-search-registries = ["example.com"]
unqualified-search-registries = ["docker.io"]

～

[regstries.search]
registries=["registry.access.redhat.com", "registry.fedoraproject.org", "docker.io"]

```